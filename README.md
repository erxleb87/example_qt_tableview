# Example: Qt TableView

This is an example for a table that can be updated and changed by clicking a button.
Using QML and Pyside6.

## Useful Links

* https://doc.qt.io/qt-6/qabstracttablemodel.html
* https://doc.qt.io/qt-6/qml-qtquick-tableview.html
* https://www.pythonguis.com/tutorials/pyside6-modelview-architecture/
* https://doc.qt.io/qt-6/qtquick-tableview-gameoflife-example.html
