"""
This is the custom table model that we use
"""
from typing import Union
from pandas import DataFrame
from PySide6.QtCore import (
    QAbstractTableModel, QPersistentModelIndex, QModelIndex, Qt, Slot
)


class MyCustomModel(QAbstractTableModel):
    INDEX_COLUMN = ["Monday", "Tuesday", "Wednesday"]

    def __init__(self):
        super().__init__()
        self._data = DataFrame(
            data={
                "Values": [10, 20, 30],
                "Other Values": [11, 22, 33]
            },
            index=self.INDEX_COLUMN
        )
        print(self._data)

    # Required overrides
    def rowCount(
        self,
        parent: Union[QModelIndex, QPersistentModelIndex] = QModelIndex()
    ) -> int:
        return len(self._data.index)

    def columnCount(
        self,
        parent: Union[QModelIndex, QPersistentModelIndex] = QModelIndex()
    ) -> int:
        return len(self._data.columns)

    def data(
        self,
        index: Union[QModelIndex, QPersistentModelIndex],
        role: int = Qt.DisplayRole
    ) -> int:
        if not index.isValid():
            raise IndexError(f"No data for invalid index {index}")
        return int(  # This cast here is important. See (1)
            self._data.iloc[index.row(), index.column()]
        )
    # (1) Not having the cast in this place makes Qt guess,
    # that the type should be QString, which PySide can't handle
    # (Or something along these lines)

    def setData(
        self,
        index: Union[QModelIndex, QPersistentModelIndex],
        value: int,
        role: int = Qt.DisplayRole
    ) -> bool:

        if not index.isValid():
            return False

        # Do the actual update
        self._data.iloc[index.row(), index.column()] = value
        # Emit this signal to trigger a view update
        self.dataChanged.emit(index, index, role)
        return True

    def headerData(
        self,
        section: int,
        orientation: Qt.Orientation,
        role: int = Qt.DisplayRole
    ) -> str:
        if orientation == Qt.Orientation.Horizontal:
            return self._data.columns.values[section]
        else:
            return self._data.index.values[section]

    def setHeaderData(
        self,
        section: int,
        orientation: Qt.Orientation,
        value: str,
        role: int = Qt.DisplayRole
    ) -> bool:
        if orientation == Qt.Orientation.Horizontal:
            self._data.columns.values[section] = value
        else:
            self._data.index.values[section] = value
        # Let the UI know about the change
        self.headerDataChanged.emit(orientation, section, section)
        return True

    # My own slots
    @Slot()
    def on_button_click(self):
        new_value = self._data.iloc[0, 0] + 1
        self.setData(self.index(0, 0),  new_value)

    @Slot()
    def on_button1_click(self):
        
        self.layoutAboutToBeChanged.emit()

        df = DataFrame(
            data={
                "A": [4, 5, 6, 1],
                "B": [1, 2, 3, 3],
                "C": [9, 8, 7, 5],
                "D": [3, 4, 5, 7]
            },
            index=["row 1", "row 2", "row 3", "row 4"]
        )
        
        self.update(df)

    @Slot()
    def on_button2_click(self):

        df = DataFrame(
            data={
                "A": [0, 1],
            },
            index=["row A", "row B"]
        )
        self.update(df)
        
    
    def update(self, df):
        self.layoutAboutToBeChanged.emit()
        self._data = df
        self.dataChanged.emit(self.index(0,0), self.index(1,1), Qt.DisplayRole)
        self.layoutChanged.emit()
