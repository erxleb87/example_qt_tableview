// This is a custom element for the Horizontal Header Cells

import QtQuick 2.15

Rectangle{
    implicitWidth: 150
    implicitHeight: 20
    color: "light blue"

    Text {
        text: display
        anchors.centerIn: parent
    }
}
