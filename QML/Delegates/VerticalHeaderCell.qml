// This is a custom element for the Vertical Header Cells

import QtQuick 2.15

Rectangle{
    implicitWidth: 150
    implicitHeight: 20
    color: "light yellow"

    Text {
        text: display
        anchors.right: parent.right
        // Add a little padding so the text is not right at the border
        rightPadding: 10
    }
}
