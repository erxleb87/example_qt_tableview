"""
An example on how to set up an interactive QTableView with a custom model.
"""

from PySide6.QtCore import QObject, QUrl, Slot
from PySide6.QtGui import QGuiApplication
from PySide6.QtQml import QQmlApplicationEngine

from custom_model import MyCustomModel
from pathlib import Path

if __name__ == "__main__":

    # Set up the basic boilerplate
    app = QGuiApplication()
    engine = QQmlApplicationEngine()
    qml_file = Path("main.qml")

    # Set up my custom model
    my_model = MyCustomModel()
    engine.rootContext().setContextProperty("my_model", my_model)

    # Prepare for Liftoff…
    engine.load(QUrl.fromLocalFile(qml_file))

    # And action!
    exit(app.exec())
