import QtQuick
import QtQuick.Controls
import QtQuick.Layouts
import "QML/Delegates"  // Load the folder with our custom cell delegates

ApplicationWindow {
    visible: true
    width: 400
    height: 300

    TableView {
        id: my_table
        model: my_model

        width: 200
        height: 100
        anchors.centerIn: parent

        delegate: Rectangle {
            // This rectangle specifies the cell_background
            implicitHeight: 20  // See (1)
            implicitWidth:  40  // See (1)
            color: palette.base
            border.width: 1

            // (1) QML wants this to deal with the layout of empty cells

            Text {
                // This is the actually displayed value
                // on top of the cell background
                text: display
                anchors.centerIn: parent
            }
        }
    }

    HorizontalHeaderView {
        syncView: my_table
        anchors{
            // Properly anchoring the element is actually important!
            // Otherwise it will be placed independently of the table
            bottom: my_table.top
            horizontalCenter: my_table.horizontalCenter
        }

        // Use a custom delegate from QML/Delegates/HorizontalHeaderCell.qml
        // Don't forget the {} or it will not work
        delegate: HorizontalHeaderCell{}
    }

    VerticalHeaderView {
        id: vertical_header
        syncView: my_table
        anchors{
            // Properly anchoring the element is actually important!
            // Otherwise it will be placed independently of the table
            right: my_table.left
            top: my_table.top
        }
        delegate: VerticalHeaderCell{}
    }

    // Place this button last or it gets shifted into the background
    // and can't be clicked anymore

    RowLayout
    {
        Button {
            id: button
            text: "Increment data[0, 0]"
            onClicked: my_model.on_button_click()
        }

        Button {
            id: button1
            text: "Load BIG DF"
            onClicked: my_model.on_button1_click()
        }

        Button {
            id: button2
            text: "Load small df"
            onClicked: my_model.on_button2_click()
        }
    }
}